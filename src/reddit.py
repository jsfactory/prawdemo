import praw
from dotenv import load_dotenv
import os
import logging
from psaw import PushshiftAPI

load_dotenv()

# handler = logging.StreamHandler()
# handler.setLevel(logging.DEBUG)
# for logger_name in ("praw", "prawcore"):
#     logger = logging.getLogger(logger_name)
#     logger.setLevel(logging.DEBUG)
#     logger.addHandler(handler)

reddit = praw.Reddit(
    client_id=os.environ.get('CLIENT_ID'),
    client_secret=os.environ.get('CLIENT_SECRET'),
    user_agent=os.environ.get('USER_AGENT'),
    username=os.environ.get('REDDIT_USERNAME'),
    password=os.environ.get('REDDIT_PASSWORD'),
    ratelimit_seconds=os.environ.get('RATE_LIMIT_SECONDS')
)

api = PushshiftAPI(reddit)