import os
from reddit import reddit, api
from config import subreddits, keywords
from datetime import datetime
from pymongo import MongoClient
from praw.models import MoreComments
import sys
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument('sub', type=str, help='subreddit name')
parser.add_argument("--new", action='store_true',
                    default=True, help="fetch new posts")
parser.add_argument("--hot", action='store_true', help="fetch hot posts")
parser.add_argument("--top", action='store_true', help="fetch top posts")
parser.add_argument("--limit", type=int, action='store',
                    help="fetch top posts")

args = parser.parse_args()
print(args)

client = MongoClient(os.environ.get('MONGODB_URL'))
# client = MongoClient(os.environ.get('MONGODB_URL'), username=os.environ.get(
#     'MONGO_INITDB_ROOT_USERNAME'), password=os.environ.get('MONGO_INITDB_ROOT_PASSWORD'))
db = client['reddit']
posts_col = db[args.sub+"_posts"]
comments_col = db[args.sub+"_comments"]

# sub, *params = sys.argv[1:]
# limit = int(params[0]) if params else None

# my_subreddits = reddit.subreddit(args.sub)

# req = my_subreddits.top(
#     limit=args.limit) if args.top else my_subreddits.hot(limit=args.limit) if args.hot else my_subreddits.new(limit=args.limit)

req = api.search_submissions(subreddit=args.sub, limit=args.limit if args.limit else None)

for submission in req:
    _post = {
        "id": submission.id,
        "author": '[deleted]' if not submission.author else submission.author.name,
        "url": submission.url,
        "title": submission.title,
        "body": submission.selftext,
        # "subreddit": submission.subreddit,
        "created_utc": datetime.fromtimestamp(submission.created_utc, None),
        "num_comments": submission.num_comments,
        "score": submission.score,
        "upvote_ratio": submission.upvote_ratio,
        "subreddit": submission.subreddit.display_name
        # "comments": comments
    }

    print(submission.id)

    posts_col.update_one({"id": submission.id}, {'$set': _post}, upsert=True)

    # _comments = []
    for comment in submission.comments:
        if isinstance(comment, MoreComments):
            continue
        _comment = {
            "id": comment.id,
            "author": '[deleted]' if not comment.author else comment.author.name,
            "body": comment.body,
            "created_utc": datetime.fromtimestamp(comment.created_utc, None),
            "score": comment.score,
            "link_id": comment.link_id,
            "parent_id": comment.parent_id,
            "subreddit": comment.subreddit.display_name,
            "submission": submission.id
        }
        comments_col.update_one({"id": comment.id}, {
                                '$set': _comment}, upsert=True)
        # _comments.append(_comment)
