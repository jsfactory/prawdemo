import json
import requests
from reddit import reddit

# subreddit = reddit.subreddit("ocd")

# print(subreddit.display_name)
# print(subreddit.title)
# print(subreddit.description)


# for submission in subreddit.top(limit=1):
#     submission.comments.replace_more(limit=0)
#     for comment in submission.comments:
#         print(comment.body)
#         for reply in comment.replies:
#             print(f'>>>>>> {reply.body}')


# for submission in subreddit.stream.submissions():
#     # print(f'{submission.author.name} : {submission.title}')
#     if "tips" in submission.title:
#         print(f'found {submission.id} {submission.url}')
#         reply_text = "thank you"
#         submission.reply(reply_text)

# data = {}
# data['posts'] = []

# for submission in subreddit.hot(limit=3):
#     submission.comments.replace_more(limit=0)
#     comments = []
#     for comment in submission.comments:
#         comments.append(
#             {"id": comment.id, "score": comment.score, "created_at": comment.created_utc, "body": comment.body, })
#         replies = []
#         for reply in comment.replies:
#             # print(f'>>>>>> {reply.body}')
#             replies.append(reply.body)

#     data['posts'].append({
#         "id": submission.id,
#         "author": submission.author.name,
#         "title": submission.title,
#         "score": submission.score,
#         "created": submission.created_utc,
#         "url": submission.url,
#         "body": submission.selftext,
#         "comments": comments
#     })

# with open('output/data.json', 'w') as outfile:
#     json.dump(data, outfile)

subreddit_name = "selfie"
subreddit = reddit.subreddit(subreddit_name)

for submission in subreddit.hot(limit=50):
    url = submission.url
    if submission.url.endswith(('.jpg', '.png', '.gif', '.jpeg')):
        print(url)
        response = requests.get(url)
        file = open(f"output/{subreddit_name}-{submission.id}.png", "wb")
        file.write(response.content)
        file.close()

# subreddit = reddit.subreddit("depression")

# for submission in subreddit.hot(limit=3):
#     print(f'{submission.title} - {submission.selftext[:100]} ...')
