import requests
from reddit import reddit, api
from pymongo import MongoClient
import os
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--limit", type=int, action='store',
                    help="limit")
args = parser.parse_args()

client = MongoClient(os.environ.get('MONGODB_URL'))
db = client['app']
selfies_col = db["selfies"]

# subreddit = reddit.subreddit("selfie")
req = api.search_submissions(subreddit="selfie", limit=args.limit if args.limit else None)

for submission in req:
    url = submission.url
    if submission.author is not None:
        print(submission.author)
        if submission.url.endswith(('.jpg', '.png', '.gif', '.jpeg')):
            response = requests.get(url)
            filename = f'{submission.subreddit.display_name}-{submission.author.name}-{submission.id}.jpg'
            file = open(f"output/{filename}", "wb")
            file.write(response.content)
            file.close()
            selfies_col.update_one({"id": submission.id}, {
                                '$set': {"title": submission.title, "filename": filename}}, upsert=True)
