subreddits = ['ocd', 'adhd', 'pureo', 'realEventOCD',
              'OCDmemes', 'MaladaptiveDreaming', 'ROCD', 'transOCD', 'HOCD', 'OCPD', 'socialanxiety']

keywords = ['rsd', 'rejection sensitivity dysphoria',
            'rejection sensitive dysphoria']
# 'rejection', 'sensitivity', 'sensitive'
