import requests
from reddit import reddit, api
from pymongo import MongoClient
import os
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--limit", type=int, action='store',
                    help="limit")
args = parser.parse_args()

client = MongoClient(os.environ.get('MONGODB_URL'))
db = client['app']
selfies_col = db["selfies"]
redditors_col = db["redditors"]

subreddit = 'selfie'

req = api.search_submissions(
    subreddit=subreddit, limit=args.limit if args.limit else None)

for submission in req:
    if submission.author is not None:
        print(submission.author)
        author = submission.author.name
        redditors_col.update_one({"username": author}, {
            '$set': {
                "avatar": submission.author.icon_img, "created_utc": submission.author.icon_img},
        }, upsert=True)

        directory = f"output/{author}"
        if not os.path.exists(directory):
            os.makedirs(directory)
        for post in api.search_submissions(subreddit=subreddit, author=author):
            url = post.url
            # print(url)
            if post.url.endswith(('.jpg', '.png', '.gif', '.jpeg')):
                response = requests.get(url)
                filename = f'{author}-{post.id}-{post.created_utc}.jpg'
                file = open(f"{directory}/{filename}", "wb")
                file.write(response.content)
                file.close()
                selfies_col.update_one({"id": post.id}, {
                    '$set': {"author": author, "title": post.title, "filename": filename, "url": post.url, "subreddit": post.subreddit.display_name, "over_18": post.over_18}}, upsert=True)
