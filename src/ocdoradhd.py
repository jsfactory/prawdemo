from reddit import reddit
from config import subreddits, keywords
from datetime import datetime
from pymongo import MongoClient
import os

client = MongoClient(os.environ.get('MONGODB_URL'))
db = client['app']
posts = db["posts"]

# exit()

# if 'posts' not in db.list_collection_names():
#     db.createCollection("posts")
#     posts = db.posts

# my_subreddits = reddit.subreddit('+'.join(subreddits))
my_subreddits = reddit.subreddit('adhd')

for submission in my_subreddits.search('ocd adhd', limit=2):

    comments = []
    for comment in submission.comments:
        comments.append({
            "id": comment.id,
            "author": '[deleted]' if not comment.author else comment.author.name,
            "body": comment.body,
            "created_utc": datetime.fromtimestamp(comment.created_utc, None)
        })

    post = {
        "id": submission.id,
        "author": '[deleted]' if not submission.author else submission.author.name,
        "url": submission.url,
        "title": submission.title,
        "body": submission.selftext,
        # "subreddit": submission.subreddit,
        "created_utc": datetime.fromtimestamp(submission.created_utc, None),
        "num_comments": submission.num_comments,
        "score": submission.score,
        "upvote_ratio": submission.upvote_ratio,
        "comments": comments
    }

    posts.update_one({"id": submission.id}, {'$set': post}, upsert=True)
    # print(f'inserted {post_id}')
    # print(submission.title)
    # print(submission.url)
    # print(datetime.fromtimestamp(submission.created_utc).strftime(
    #     "%A, %B %d, %Y %I:%M:%S"))

# for submission in my_subreddits.new(limit=100):
#     if(any(keyword in submission.selftext for keyword in keywords)):
#         print('found it')
#         print(submission.url)
#         # for comment in submission.comments:
#         #     print(comment.body)
#         #     for reply in comment.replies:
#         #         print(f'>>>>>> {reply.body}')
