> docker build -t praw .

> docker run -it -v $PWD:/app -w /app praw
> docker run -it -v $PWD:/app praw python src/demo.py
> docker run -it -v $PWD:/app praw python src/ocdoradhd.py
> docker run -it -v $PWD:/app praw python src/feed.py

> docker-compose run crawler python src/feed.py
> docker-compose run crawler python src/feed.py --top --limit 1 memes
> docker-compose run -d crawler python src/feed.py adhd
> docker-compose run -d crawler python src/feed.py ocd
> docker-compose run -d crawler python src/feed.py pureo
> docker-compose run -d crawler python src/profiles.py --limit 2

> mongoexport --collection=ocd_comments --db=reddit --fields=body,subreddit --out=/data/db/ocd_comments.csv
> mongoexport --collection=ocd_posts --db=reddit --fields=body,subreddit --out=/data/db/ocd_posts.csv
> mongoexport --collection=pureo_comments --db=reddit --fields=body,subreddit --out=/data/db/pureo_comments.csv
> mongoexport --collection=pureo_posts --db=reddit --fields=body,subreddit --out=/data/db/pure_posts.csv
> mongoexport --collection=adhd_comments --db=reddit --fields=body,subreddit --out=/data/db/adhd_comments.csv
> mongoexport --collection=adhd_posts --db=reddit --fields=body,subreddit --out=/data/db/adhd_posts.csv


> mongoexport --collection=ocd_posts --db=reddit --fields=id,author,url,title,body,created_utc,num_comments,score,upvote_ratio,subreddit --out=/data/db/ocd_posts.csv
> mongoexport --collection=ocd_comments --db=reddit --fields=id,author,url,title,body,created_utc,num_comments,score,upvote_ratio,subreddit --out=/data/db/ocd_comments.csv
> mongoexport --collection=adhd_posts --db=reddit --fields=id,author,url,title,body,created_utc,num_comments,score,upvote_ratio,subreddit --out=/data/db/adhd_posts.csv
> mongoexport --collection=adhd_comments --db=reddit --fields=id,author,url,title,body,created_utc,num_comments,score,upvote_ratio,subreddit --out=/data/db/adhd_comments.csv
> mongoexport --collection=pureo_posts --db=reddit --fields=id,author,url,title,body,created_utc,num_comments,score,upvote_ratio,subreddit --out=/data/db/pureo_posts.csv
> mongoexport --collection=pureo_comments --db=reddit --fields=id,author,url,title,body,created_utc,num_comments,score,upvote_ratio,subreddit --out=/data/db/pureo_comments.csv

> scp -r root@134.122.12.221:~/prawdemo/data ~/Desktop/data/datasets

> ls output | wc -l